#include <Arduino.h>
#include <FreeRTOS.h>
#include "DShotRMT.h"
#include "sigProc.h"
#include <math.h>
#include "MPU6050_6Axis_MotionApps612.h"

//
// PINOUT DEFINITIONS
//

#define LED_BUILTIN 2

#define M1_pin 26
#define M2_pin 27
#define M3_pin 14
#define M4_pin 25

struct motor {
  uint16_t speed = 0;
  DShotRMT dshot;
  bool reversed;
  uint8_t rmt_channel;
  uint8_t gpio_pin;
  bool ready = false;
  bool standby = true;
  String name;
  bool rmt_init = false;
} M1, M2, M3, M4;

//
// Setup and loop
//

void motorInit(void *pvParameters) {
  vTaskDelay(1 / portTICK_PERIOD_MS);
  ((motor*)pvParameters)->rmt_init = !((motor*)pvParameters)->dshot.install(
    gpio_num_t(((motor*)pvParameters)->gpio_pin),
    rmt_channel_t(((motor*)pvParameters)->rmt_channel)
    );
  if (((motor*)pvParameters)->rmt_init){         
    ((motor*)pvParameters)->dshot.init(true);
    ((motor*)pvParameters)->dshot.setReversed(((motor*)pvParameters)->reversed);
    const TickType_t xFrequency = 100;
    TickType_t xLastWakeTime = xTaskGetTickCount();
    vTaskDelayUntil(&xLastWakeTime, int(1000 / xFrequency));
    while (((motor*)pvParameters)->standby) {
      uint16_t speed = ((motor*)pvParameters)->speed;
      if (speed < 48) speed = 48; if (speed > 2047) speed = 2047;
      ((motor*)pvParameters)->ready = true;
      ((motor*)pvParameters)->dshot.sendThrottle(speed);
      vTaskDelayUntil(&xLastWakeTime, int(1000 / xFrequency));
    }
  } vTaskDelete( NULL );
}

void motorGovnr(void *pvParameters) {
  const TickType_t xFrequency = 400;
  TickType_t xLastWakeTime = xTaskGetTickCount();
  // vTaskDelay(10000/portTICK_PERIOD_MS);
  while(true){
    if (M1.ready) {M1.standby = 0; M1.dshot.sendThrottle(M1.speed);}
    if (M2.ready) {M2.standby = 0; M2.dshot.sendThrottle(M2.speed);}
    if (M3.ready) {M3.standby = 0; M3.dshot.sendThrottle(M3.speed);}
    if (M4.ready) {M4.standby = 0; M4.dshot.sendThrottle(M4.speed);}

    vTaskDelayUntil(&xLastWakeTime, int(1000 / xFrequency));
  }
}

void setup() {

  // Initialize serial (USB)
  Serial.begin(115200);
  while (!Serial){
    delay(10);
  } Serial.println("Serial initialized");

  M1.name = "M1";
  M2.name = "M2";
  M3.name = "M3";
  M4.name = "M4";

  M1.speed = 100;
  M2.speed = 100;
  M3.speed = 100;
  M4.speed = 100;

  M1.reversed = true;
  M2.reversed = true;
  M3.reversed = false;
  M4.reversed = false;

  M1.gpio_pin = M1_pin;
  M2.gpio_pin = M2_pin;
  M3.gpio_pin = M3_pin;
  M4.gpio_pin = M4_pin;

  M1.rmt_channel = 0;
  M2.rmt_channel = 1;
  M3.rmt_channel = 2;
  M4.rmt_channel = 3;

  xTaskCreate(motorInit, "motorGovM1", 5000, &M1, 1, NULL);
  xTaskCreate(motorInit, "motorGovM2", 5000, &M2, 1, NULL);
  xTaskCreate(motorInit, "motorGovM3", 5000, &M3, 1, NULL);
  xTaskCreate(motorInit, "motorGovM4", 5000, &M4, 1, NULL);

  xTaskCreate(motorGovnr, "motorAfter", 5000, NULL, 1, NULL);

}

void loop() {

  vTaskDelay(1000/portTICK_PERIOD_MS);

}
