#include <math.h>
#include "untitled_acc.h"
#include "untitled_acc_private.h"
#include <stdio.h>
#include "slexec_vm_simstruct_bridge.h"
#include "slexec_vm_zc_functions.h"
#include "slexec_vm_lookup_functions.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "simtarget/slSimTgtMdlrefSfcnBridge.h"
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
#include "simtarget/slAccSfcnBridge.h"
#ifndef __RTW_UTFREE__  
extern void * utMalloc ( size_t ) ; extern void utFree ( void * ) ;
#endif
boolean_T untitled_acc_rt_TDelayUpdateTailOrGrowBuf ( int_T * bufSzPtr ,
int_T * tailPtr , int_T * headPtr , int_T * lastPtr , real_T tMinusDelay ,
real_T * * uBufPtr , boolean_T isfixedbuf , boolean_T istransportdelay ,
int_T * maxNewBufSzPtr ) { int_T testIdx ; int_T tail = * tailPtr ; int_T
bufSz = * bufSzPtr ; real_T * tBuf = * uBufPtr + bufSz ; real_T * xBuf = (
NULL ) ; int_T numBuffer = 2 ; if ( istransportdelay ) { numBuffer = 3 ; xBuf
= * uBufPtr + 2 * bufSz ; } testIdx = ( tail < ( bufSz - 1 ) ) ? ( tail + 1 )
: 0 ; if ( ( tMinusDelay <= tBuf [ testIdx ] ) && ! isfixedbuf ) { int_T j ;
real_T * tempT ; real_T * tempU ; real_T * tempX = ( NULL ) ; real_T * uBuf =
* uBufPtr ; int_T newBufSz = bufSz + 1024 ; if ( newBufSz > * maxNewBufSzPtr
) { * maxNewBufSzPtr = newBufSz ; } tempU = ( real_T * ) utMalloc ( numBuffer
* newBufSz * sizeof ( real_T ) ) ; if ( tempU == ( NULL ) ) { return ( false
) ; } tempT = tempU + newBufSz ; if ( istransportdelay ) tempX = tempT +
newBufSz ; for ( j = tail ; j < bufSz ; j ++ ) { tempT [ j - tail ] = tBuf [
j ] ; tempU [ j - tail ] = uBuf [ j ] ; if ( istransportdelay ) tempX [ j -
tail ] = xBuf [ j ] ; } for ( j = 0 ; j < tail ; j ++ ) { tempT [ j + bufSz -
tail ] = tBuf [ j ] ; tempU [ j + bufSz - tail ] = uBuf [ j ] ; if (
istransportdelay ) tempX [ j + bufSz - tail ] = xBuf [ j ] ; } if ( * lastPtr
> tail ) { * lastPtr -= tail ; } else { * lastPtr += ( bufSz - tail ) ; } *
tailPtr = 0 ; * headPtr = bufSz ; utFree ( uBuf ) ; * bufSzPtr = newBufSz ; *
uBufPtr = tempU ; } else { * tailPtr = testIdx ; } return ( true ) ; } real_T
untitled_acc_rt_TDelayInterpolate ( real_T tMinusDelay , real_T tStart ,
real_T * uBuf , int_T bufSz , int_T * lastIdx , int_T oldestIdx , int_T
newIdx , real_T initOutput , boolean_T discrete , boolean_T
minorStepAndTAtLastMajorOutput ) { int_T i ; real_T yout , t1 , t2 , u1 , u2
; real_T * tBuf = uBuf + bufSz ; if ( ( newIdx == 0 ) && ( oldestIdx == 0 )
&& ( tMinusDelay > tStart ) ) return initOutput ; if ( tMinusDelay <= tStart
) return initOutput ; if ( ( tMinusDelay <= tBuf [ oldestIdx ] ) ) { if (
discrete ) { return ( uBuf [ oldestIdx ] ) ; } else { int_T tempIdx =
oldestIdx + 1 ; if ( oldestIdx == bufSz - 1 ) tempIdx = 0 ; t1 = tBuf [
oldestIdx ] ; t2 = tBuf [ tempIdx ] ; u1 = uBuf [ oldestIdx ] ; u2 = uBuf [
tempIdx ] ; if ( t2 == t1 ) { if ( tMinusDelay >= t2 ) { yout = u2 ; } else {
yout = u1 ; } } else { real_T f1 = ( t2 - tMinusDelay ) / ( t2 - t1 ) ;
real_T f2 = 1.0 - f1 ; yout = f1 * u1 + f2 * u2 ; } return yout ; } } if (
minorStepAndTAtLastMajorOutput ) { if ( newIdx != 0 ) { if ( * lastIdx ==
newIdx ) { ( * lastIdx ) -- ; } newIdx -- ; } else { if ( * lastIdx == newIdx
) { * lastIdx = bufSz - 1 ; } newIdx = bufSz - 1 ; } } i = * lastIdx ; if (
tBuf [ i ] < tMinusDelay ) { while ( tBuf [ i ] < tMinusDelay ) { if ( i ==
newIdx ) break ; i = ( i < ( bufSz - 1 ) ) ? ( i + 1 ) : 0 ; } } else { while
( tBuf [ i ] >= tMinusDelay ) { i = ( i > 0 ) ? i - 1 : ( bufSz - 1 ) ; } i =
( i < ( bufSz - 1 ) ) ? ( i + 1 ) : 0 ; } * lastIdx = i ; if ( discrete ) {
double tempEps = ( DBL_EPSILON ) * 128.0 ; double localEps = tempEps *
muDoubleScalarAbs ( tBuf [ i ] ) ; if ( tempEps > localEps ) { localEps =
tempEps ; } localEps = localEps / 2.0 ; if ( tMinusDelay >= ( tBuf [ i ] -
localEps ) ) { yout = uBuf [ i ] ; } else { if ( i == 0 ) { yout = uBuf [
bufSz - 1 ] ; } else { yout = uBuf [ i - 1 ] ; } } } else { if ( i == 0 ) {
t1 = tBuf [ bufSz - 1 ] ; u1 = uBuf [ bufSz - 1 ] ; } else { t1 = tBuf [ i -
1 ] ; u1 = uBuf [ i - 1 ] ; } t2 = tBuf [ i ] ; u2 = uBuf [ i ] ; if ( t2 ==
t1 ) { if ( tMinusDelay >= t2 ) { yout = u2 ; } else { yout = u1 ; } } else {
real_T f1 = ( t2 - tMinusDelay ) / ( t2 - t1 ) ; real_T f2 = 1.0 - f1 ; yout
= f1 * u1 + f2 * u2 ; } } return ( yout ) ; } void rt_ssGetBlockPath (
SimStruct * S , int_T sysIdx , int_T blkIdx , char_T * * path ) {
_ssGetBlockPath ( S , sysIdx , blkIdx , path ) ; } void rt_ssSet_slErrMsg (
void * S , void * diag ) { SimStruct * castedS = ( SimStruct * ) S ; if ( !
_ssIsErrorStatusAslErrMsg ( castedS ) ) { _ssSet_slErrMsg ( castedS , diag )
; } else { _ssDiscardDiagnostic ( castedS , diag ) ; } } void
rt_ssReportDiagnosticAsWarning ( void * S , void * diag ) {
_ssReportDiagnosticAsWarning ( ( SimStruct * ) S , diag ) ; } void
rt_ssReportDiagnosticAsInfo ( void * S , void * diag ) {
_ssReportDiagnosticAsInfo ( ( SimStruct * ) S , diag ) ; } static void
mdlOutputs ( SimStruct * S , int_T tid ) { B_untitled_T * _rtB ;
DW_untitled_T * _rtDW ; P_untitled_T * _rtP ; X_untitled_T * _rtX ; int32_T
isHit ; uint32_T ri ; _rtDW = ( ( DW_untitled_T * ) ssGetRootDWork ( S ) ) ;
_rtX = ( ( X_untitled_T * ) ssGetContStates ( S ) ) ; _rtP = ( ( P_untitled_T
* ) ssGetModelRtp ( S ) ) ; _rtB = ( ( B_untitled_T * ) _ssGetModelBlockIO (
S ) ) ; _rtB -> B_0_0_0 = 0.0 ; for ( ri = _rtP -> P_23 [ 0U ] ; ri < _rtP ->
P_23 [ 1U ] ; ri ++ ) { _rtB -> B_0_0_0 += _rtP -> P_2 * _rtX ->
Internal_CSTATE [ 0U ] ; } for ( ri = _rtP -> P_23 [ 1U ] ; ri < _rtP -> P_23
[ 2U ] ; ri ++ ) { _rtB -> B_0_0_0 += _rtP -> P_2 * _rtX -> Internal_CSTATE [
1U ] ; } isHit = ssIsSampleHit ( S , 1 , 0 ) ; if ( isHit != 0 ) { { if (
_rtDW -> TAQSigLogging_InsertedFor_LTISystem_at_outport_0_PWORK . AQHandles
&& ssGetLogOutput ( S ) ) { sdiWriteSignal ( _rtDW ->
TAQSigLogging_InsertedFor_LTISystem_at_outport_0_PWORK . AQHandles ,
ssGetTaskTime ( S , 1 ) , ( char * ) & _rtB -> B_0_0_0 + 0 ) ; } } } { real_T
* * uBuffer = ( real_T * * ) & _rtDW -> TransportDelay_PWORK . TUbufferPtrs [
0 ] ; real_T simTime = ssGetT ( S ) ; real_T tMinusDelay = simTime - _rtP ->
P_4 ; _rtB -> B_0_2_0 = untitled_acc_rt_TDelayInterpolate ( tMinusDelay , 0.0
, * uBuffer , _rtDW -> TransportDelay_IWORK . CircularBufSize , & _rtDW ->
TransportDelay_IWORK . Last , _rtDW -> TransportDelay_IWORK . Tail , _rtDW ->
TransportDelay_IWORK . Head , _rtP -> P_5 , 0 , ( boolean_T ) (
ssIsMinorTimeStep ( S ) && ( ssGetTimeOfLastOutput ( S ) == ssGetT ( S ) ) )
) ; } _rtB -> B_0_3_0 = _rtB -> B_0_2_0 - _rtB -> B_0_0_0_m ; isHit =
ssIsSampleHit ( S , 1 , 0 ) ; if ( isHit != 0 ) { { if ( _rtDW ->
TAQSigLogging_InsertedFor_Sum1_at_outport_0_PWORK . AQHandles &&
ssGetLogOutput ( S ) ) { sdiWriteSignal ( _rtDW ->
TAQSigLogging_InsertedFor_Sum1_at_outport_0_PWORK . AQHandles , ssGetTaskTime
( S , 1 ) , ( char * ) & _rtB -> B_0_3_0 + 0 ) ; } } _rtDW -> Step_MODE = (
ssGetTaskTime ( S , 1 ) >= _rtP -> P_6 ) ; if ( _rtDW -> Step_MODE == 1 ) {
_rtB -> B_0_5_0 = _rtP -> P_8 ; } else { _rtB -> B_0_5_0 = _rtP -> P_7 ; } }
_rtB -> B_0_6_0 = _rtB -> B_0_5_0 - _rtB -> B_0_0_0 ; isHit = ssIsSampleHit (
S , 1 , 0 ) ; if ( isHit != 0 ) { { if ( _rtDW ->
TAQSigLogging_InsertedFor_Sum2_at_outport_0_PWORK . AQHandles &&
ssGetLogOutput ( S ) ) { sdiWriteSignal ( _rtDW ->
TAQSigLogging_InsertedFor_Sum2_at_outport_0_PWORK . AQHandles , ssGetTaskTime
( S , 1 ) , ( char * ) & _rtB -> B_0_6_0 + 0 ) ; } } } _rtB -> B_0_8_0 = _rtP
-> P_9 * _rtB -> B_0_6_0 ; _rtB -> B_0_9_0 = _rtX -> Filter_CSTATE ; _rtB ->
B_0_10_0 = _rtB -> B_0_8_0 - _rtB -> B_0_9_0 ; _rtB -> B_0_11_0 = _rtP ->
P_11 * _rtB -> B_0_6_0 ; _rtB -> B_0_12_0 = _rtX -> Integrator_CSTATE ; _rtB
-> B_0_13_0 = _rtP -> P_13 * _rtB -> B_0_10_0 ; _rtB -> B_0_14_0 = ( _rtB ->
B_0_6_0 + _rtB -> B_0_12_0 ) + _rtB -> B_0_13_0 ; _rtB -> B_0_15_0 = _rtP ->
P_14 * _rtB -> B_0_14_0 ; if ( ssIsMajorTimeStep ( S ) != 0 ) { _rtDW ->
Saturation_MODE = _rtB -> B_0_15_0 >= _rtP -> P_15 ? 1 : _rtB -> B_0_15_0 >
_rtP -> P_16 ? 0 : - 1 ; } _rtB -> B_0_16_0 = _rtDW -> Saturation_MODE == 1 ?
_rtP -> P_15 : _rtDW -> Saturation_MODE == - 1 ? _rtP -> P_16 : _rtB ->
B_0_15_0 ; UNUSED_PARAMETER ( tid ) ; } static void mdlOutputsTID2 (
SimStruct * S , int_T tid ) { B_untitled_T * _rtB ; P_untitled_T * _rtP ;
_rtP = ( ( P_untitled_T * ) ssGetModelRtp ( S ) ) ; _rtB = ( ( B_untitled_T *
) _ssGetModelBlockIO ( S ) ) ; _rtB -> B_0_0_0_m = _rtP -> P_17 ;
UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { B_untitled_T * _rtB ;
DW_untitled_T * _rtDW ; P_untitled_T * _rtP ; _rtDW = ( ( DW_untitled_T * )
ssGetRootDWork ( S ) ) ; _rtP = ( ( P_untitled_T * ) ssGetModelRtp ( S ) ) ;
_rtB = ( ( B_untitled_T * ) _ssGetModelBlockIO ( S ) ) ; { real_T * * uBuffer
= ( real_T * * ) & _rtDW -> TransportDelay_PWORK . TUbufferPtrs [ 0 ] ;
real_T simTime = ssGetT ( S ) ; _rtDW -> TransportDelay_IWORK . Head = ( (
_rtDW -> TransportDelay_IWORK . Head < ( _rtDW -> TransportDelay_IWORK .
CircularBufSize - 1 ) ) ? ( _rtDW -> TransportDelay_IWORK . Head + 1 ) : 0 )
; if ( _rtDW -> TransportDelay_IWORK . Head == _rtDW -> TransportDelay_IWORK
. Tail ) { if ( ! untitled_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
TransportDelay_IWORK . CircularBufSize , & _rtDW -> TransportDelay_IWORK .
Tail , & _rtDW -> TransportDelay_IWORK . Head , & _rtDW ->
TransportDelay_IWORK . Last , simTime - _rtP -> P_4 , uBuffer , ( boolean_T )
0 , false , & _rtDW -> TransportDelay_IWORK . MaxNewBufSize ) ) {
ssSetErrorStatus ( S , "tdelay memory allocation error" ) ; return ; } } ( *
uBuffer + _rtDW -> TransportDelay_IWORK . CircularBufSize ) [ _rtDW ->
TransportDelay_IWORK . Head ] = simTime ; ( * uBuffer ) [ _rtDW ->
TransportDelay_IWORK . Head ] = _rtB -> B_0_16_0 ; } UNUSED_PARAMETER ( tid )
; }
#define MDL_UPDATE
static void mdlUpdateTID2 ( SimStruct * S , int_T tid ) { UNUSED_PARAMETER (
tid ) ; }
#define MDL_DERIVATIVES
static void mdlDerivatives ( SimStruct * S ) { B_untitled_T * _rtB ;
P_untitled_T * _rtP ; XDot_untitled_T * _rtXdot ; X_untitled_T * _rtX ;
uint32_T ri ; _rtXdot = ( ( XDot_untitled_T * ) ssGetdX ( S ) ) ; _rtX = ( (
X_untitled_T * ) ssGetContStates ( S ) ) ; _rtP = ( ( P_untitled_T * )
ssGetModelRtp ( S ) ) ; _rtB = ( ( B_untitled_T * ) _ssGetModelBlockIO ( S )
) ; _rtXdot -> Internal_CSTATE [ 0 ] = 0.0 ; _rtXdot -> Internal_CSTATE [ 1 ]
= 0.0 ; for ( ri = _rtP -> P_19 [ 0U ] ; ri < _rtP -> P_19 [ 1U ] ; ri ++ ) {
_rtXdot -> Internal_CSTATE [ _rtP -> P_18 [ ri ] ] += _rtP -> P_0 [ ri ] *
_rtX -> Internal_CSTATE [ 0U ] ; } for ( ri = _rtP -> P_19 [ 1U ] ; ri < _rtP
-> P_19 [ 2U ] ; ri ++ ) { _rtXdot -> Internal_CSTATE [ _rtP -> P_18 [ ri ] ]
+= _rtP -> P_0 [ ri ] * _rtX -> Internal_CSTATE [ 1U ] ; } for ( ri = _rtP ->
P_21 [ 0U ] ; ri < _rtP -> P_21 [ 1U ] ; ri ++ ) { _rtXdot -> Internal_CSTATE
[ _rtP -> P_20 ] += _rtP -> P_1 * _rtB -> B_0_3_0 ; } _rtXdot ->
Filter_CSTATE = _rtB -> B_0_13_0 ; _rtXdot -> Integrator_CSTATE = _rtB ->
B_0_11_0 ; }
#define MDL_ZERO_CROSSINGS
static void mdlZeroCrossings ( SimStruct * S ) { B_untitled_T * _rtB ;
P_untitled_T * _rtP ; ZCV_untitled_T * _rtZCSV ; _rtZCSV = ( ( ZCV_untitled_T
* ) ssGetSolverZcSignalVector ( S ) ) ; _rtP = ( ( P_untitled_T * )
ssGetModelRtp ( S ) ) ; _rtB = ( ( B_untitled_T * ) _ssGetModelBlockIO ( S )
) ; _rtZCSV -> Step_StepTime_ZC = ssGetT ( S ) - _rtP -> P_6 ; _rtZCSV ->
Saturation_UprLim_ZC = _rtB -> B_0_15_0 - _rtP -> P_15 ; _rtZCSV ->
Saturation_LwrLim_ZC = _rtB -> B_0_15_0 - _rtP -> P_16 ; } static void
mdlInitializeSizes ( SimStruct * S ) { ssSetChecksumVal ( S , 0 , 2582574981U
) ; ssSetChecksumVal ( S , 1 , 1998757226U ) ; ssSetChecksumVal ( S , 2 ,
3027865973U ) ; ssSetChecksumVal ( S , 3 , 3051047738U ) ; { mxArray *
slVerStructMat = NULL ; mxArray * slStrMat = mxCreateString ( "simulink" ) ;
char slVerChar [ 10 ] ; int status = mexCallMATLAB ( 1 , & slVerStructMat , 1
, & slStrMat , "ver" ) ; if ( status == 0 ) { mxArray * slVerMat = mxGetField
( slVerStructMat , 0 , "Version" ) ; if ( slVerMat == NULL ) { status = 1 ; }
else { status = mxGetString ( slVerMat , slVerChar , 10 ) ; } }
mxDestroyArray ( slStrMat ) ; mxDestroyArray ( slVerStructMat ) ; if ( (
status == 1 ) || ( strcmp ( slVerChar , "10.3" ) != 0 ) ) { return ; } }
ssSetOptions ( S , SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork (
S ) != sizeof ( DW_untitled_T ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( B_untitled_T ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } { int ssSizeofParams ;
ssGetSizeofParams ( S , & ssSizeofParams ) ; if ( ssSizeofParams != sizeof (
P_untitled_T ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetModelRtp ( S , ( real_T *
) & untitled_rtDefaultP ) ; } static void mdlInitializeSampleTimes (
SimStruct * S ) { slAccRegPrmChangeFcn ( S , mdlOutputsTID2 ) ; } static void
mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
