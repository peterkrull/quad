#ifndef RTW_HEADER_untitled_acc_h_
#define RTW_HEADER_untitled_acc_h_
#include <stddef.h>
#include <float.h>
#ifndef untitled_acc_COMMON_INCLUDES_
#define untitled_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "sl_AsyncioQueue/AsyncioQueueCAPI.h"
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "untitled_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
typedef struct { real_T B_0_0_0 ; real_T B_0_2_0 ; real_T B_0_3_0 ; real_T
B_0_5_0 ; real_T B_0_6_0 ; real_T B_0_8_0 ; real_T B_0_9_0 ; real_T B_0_10_0
; real_T B_0_11_0 ; real_T B_0_12_0 ; real_T B_0_13_0 ; real_T B_0_14_0 ;
real_T B_0_15_0 ; real_T B_0_16_0 ; real_T B_0_0_0_m ; } B_untitled_T ;
typedef struct { struct { real_T modelTStart ; } TransportDelay_RWORK ;
struct { void * AQHandles ; }
TAQSigLogging_InsertedFor_LTISystem_at_outport_0_PWORK ; struct { void *
TUbufferPtrs [ 2 ] ; } TransportDelay_PWORK ; struct { void * AQHandles ; }
TAQSigLogging_InsertedFor_Sum1_at_outport_0_PWORK ; struct { void * AQHandles
; } TAQSigLogging_InsertedFor_Sum2_at_outport_0_PWORK ; struct { int_T Tail ;
int_T Head ; int_T Last ; int_T CircularBufSize ; int_T MaxNewBufSize ; }
TransportDelay_IWORK ; int_T Step_MODE ; int_T Saturation_MODE ; char_T
pad_Saturation_MODE [ 4 ] ; } DW_untitled_T ; typedef struct { real_T
Internal_CSTATE [ 2 ] ; real_T Filter_CSTATE ; real_T Integrator_CSTATE ; }
X_untitled_T ; typedef struct { real_T Internal_CSTATE [ 2 ] ; real_T
Filter_CSTATE ; real_T Integrator_CSTATE ; } XDot_untitled_T ; typedef struct
{ boolean_T Internal_CSTATE [ 2 ] ; boolean_T Filter_CSTATE ; boolean_T
Integrator_CSTATE ; } XDis_untitled_T ; typedef struct { real_T
Internal_CSTATE [ 2 ] ; real_T Filter_CSTATE ; real_T Integrator_CSTATE ; }
CStateAbsTol_untitled_T ; typedef struct { real_T Internal_CSTATE [ 2 ] ;
real_T Filter_CSTATE ; real_T Integrator_CSTATE ; } CXPtMin_untitled_T ;
typedef struct { real_T Internal_CSTATE [ 2 ] ; real_T Filter_CSTATE ; real_T
Integrator_CSTATE ; } CXPtMax_untitled_T ; typedef struct { real_T
Step_StepTime_ZC ; real_T Saturation_UprLim_ZC ; real_T Saturation_LwrLim_ZC
; } ZCV_untitled_T ; typedef struct { ZCSigState Step_StepTime_ZCE ;
ZCSigState Saturation_UprLim_ZCE ; ZCSigState Saturation_LwrLim_ZCE ; }
PrevZCX_untitled_T ; struct P_untitled_T_ { real_T P_0 [ 3 ] ; real_T P_1 ;
real_T P_2 ; real_T P_3 ; real_T P_4 ; real_T P_5 ; real_T P_6 ; real_T P_7 ;
real_T P_8 ; real_T P_9 ; real_T P_10 ; real_T P_11 ; real_T P_12 ; real_T
P_13 ; real_T P_14 ; real_T P_15 ; real_T P_16 ; real_T P_17 ; uint32_T P_18
[ 3 ] ; uint32_T P_19 [ 3 ] ; uint32_T P_20 ; uint32_T P_21 [ 2 ] ; uint32_T
P_22 ; uint32_T P_23 [ 3 ] ; char_T pad_P_23 [ 4 ] ; } ; extern P_untitled_T
untitled_rtDefaultP ;
#endif
